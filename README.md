# Basic ROOT fit

Using ROOT's basic fitting library for histograms. This script fits simple functions to the example data. It fits a backgound model to a sub-section of the data, a signal model to a different region, then fits a combined model to the full range. See the annotated code in python/runme.py

## Running
  * source setup.sh # This is if you are running on the imperial machines
  * python python/runme.py

